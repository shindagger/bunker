#!/bin/bash

echo ""
echo -e "\033[38;5;174m ---- update and upgrade ---- \033[0m"
echo ""

# update and upgrade
sudo apt-get -y update && sudo apt-get -y upgrade && sudo apt-get -y dist-upgrade

echo ""
echo -e "\033[38;5;174m ---- ldxe and tightvncserver ---- \033[0m"
echo ""

sudo apt-get -y install xorg lxde-core tightvncserver

echo ""
echo -e "\033[38;5;174m ---- chrome ---- \033[0m"
echo ""

# chrome
sudo apt-get -y install fonts-liberation xdg-utils
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome-stable_current_amd64.deb && rm google-chrome-stable_current_amd64.deb

echo ""
echo -e "\033[38;5;174m ---- gimp ---- \033[0m"
echo ""

# gimp
sudo apt-get -y autoremove gimp gimp-plugin-registry
sudo add-apt-repository -y ppa:otto-kesselgulasch/gimp
sudo apt-get -y update
sudo apt-get -y install gimp
