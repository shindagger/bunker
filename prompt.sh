#!/bin/bash

kaws() {
 eval "caws $1 --withcreds > /dev/null"
 . ~/.cawsrc  
}

nofetch_exists="$(which neofetch)"
if [ ${#nofetch_exists} = 0 ]; then
    echo ""
    echo -e "\033[38;5;174m ---- installing neofetch ---- \033[0m"
    echo -e "\033[38;5;112m this should only happen the first time you log in. \033[0m"
    echo ""

    # neofetch
    sudo chown -R ubuntu:ubuntu /home/ubuntu/.config/
    sudo add-apt-repository ppa:dawidd0811/neofetch -y
    sudo apt update -y && sudo apt install neofetch -y
    echo "neofetch" | sudo tee -a /home/ubuntu/.profile
    source /home/ubuntu/.profile
fi

parse_git_branch() {
    local b=$(git symbolic-ref HEAD 2> /dev/null)
    if [ "${b#refs/heads/}" != "" ]
    then
        printf " \e[38;5;208m(${b#refs/heads/})\e[0m"
    fi
}
function color_path() {
    ROYGBIV=('\e[31m' '\e[38;5;208m' '\e[93m' '\e[92m' '\e[36m' '\e[94m' '\e[95m' '\e[97m' '\e[93m' '\e[38;5;208m' '\e[91m' '\e[95m' '\e[96m' '\e[34m' '\e[92m')
    explode_path=$(pwd)
    exploded=$(echo $explode_path | tr "/" "\n")
    final=""
    sep="/"
    x=0
    for part in $exploded
    do
        final+="${ROYGBIV[$x]}$sep$part\e[0m"
        x=$(expr $x + 1)
    done
    printf $final
}

if [[ -z "${AWS_DEFAULT_PROFILE}" ]]; then
    PS1='\[\033[100m\]\u\[\033[0;96m\]`parse_git_branch` \[\033[37m\]- `color_path`\[\033[37m\]\n$ \[\033[0m\]'
else
    PS1='\[\033[100m\]\u\[\033[0;96m\] aws:\[\033[92m\]$AWS_DEFAULT_PROFILE`parse_git_branch` \[\033[37m\]- `color_path`\[\033[37m\]\n$ \[\033[0m\]'
fi

CAWSRC=/home/ubuntu/.cawsrc
if [ -f "$CAWSRC" ]; then
    source /home/ubuntu/.cawsrc
fi
    
