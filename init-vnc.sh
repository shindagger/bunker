#!/bin/bash

# Start VNC to create config file
#
# NOTE: this script must be run from the EC2
tightvncserver :1

# Then stop VNC
tightvncserver -kill :1

echo "lxterminal &" >> .vnc/xstartup
echo "/usr/bin/lxsession -s LXDE &" >> .vnc/xstartup
