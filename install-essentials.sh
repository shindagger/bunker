#!/bin/bash

# UBUNTU 18.04
# mount ebs
#sudo file -s /dev/xvdh
#sudo mkfs -t ext4 /dev/xvdh
#sudo mkdir /ebs-block
#sudo mount /dev/xvdh /ebs-block
#sudo cp /etc/fstab /etc/fstab.bak
#echo "/dev/xvdh       /ebs-block   ext4    defaults,nofail        0       0" | sudo tee -a /etc/fstab

echo ""
echo -e "\033[38;5;174m ---- update and upgrade apt-get ---- \033[0m"
echo ""

# update and upgrade
sudo apt-get -y update && sudo apt-get -y upgrade

echo ""
echo -e "\033[38;5;174m ---- python3 and pip3 ---- \033[0m"
echo ""

# py3 and pip3
sudo apt-get install python3.6 -y
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.6 1
#echo "alias python=python3" | sudo tee -a /home/ubuntu/.profile
sudo apt-get -y install python3-pip
sudo ln -s /usr/bin/pip3 /usr/bin/pip

echo ""
echo -e "\033[38;5;174m ---- npm and node ---- \033[0m"
echo ""

# node and npm from ubunbtu repo
sudo apt install nodejs -y
sudo apt install npm -y
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt install nodejs -y
sudo apt install build-essential -y
sudo npm install -g create-react-app

echo ""
echo -e "\033[38;5;174m ---- yarn ---- \033[0m"
echo ""

# yarn
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update && sudo apt install yarn

echo ""
echo -e "\033[38;5;174m ---- aws cli ---- \033[0m"
echo ""

# aws cli
pip install awscli

echo ""
echo -e "\033[38;5;174m ---- terraform ---- \033[0m"
echo ""

# terraform
sudo apt-get install unzip -y
wget https://releases.hashicorp.com/terraform/0.12.16/terraform_0.12.16_linux_amd64.zip
unzip terraform_0.12.16_linux_amd64.zip
sudo cp terraform /usr/local/bin/ && rm -rf terraform*

echo ""
echo -e "\033[38;5;174m ---- docker ---- \033[0m"
echo ""

# docker
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
sudo apt update -y
apt-cache policy docker-ce
sudo apt install docker-ce -y
sudo usermod -aG docker ubuntu

echo ""
echo -e "\033[38;5;174m ---- command line tools by shindagger ---- \033[0m"
echo ""

pip install caws
sudo apt-get install tree -y
pip install s3-tree
pip install extension-swap
pip install trafficlight
pip install spin-and-heave
pip install git-client

echo ""
echo -e "\033[38;5;174m ---- other useful command line tools ---- \033[0m"
echo ""

pip install icdiff
