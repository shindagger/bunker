resource "aws_iam_instance_profile" "bunker_profile" {
  name = "${var.bunker_instance_profile_name}"
  role = "${var.ssm_ec2_role}"
}

resource "aws_security_group" "bunker_sg" {
  name = "${var.bunker_security_group_name}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = var.ssh_ingress_cidrs
  }

  # Allow outgoing traffic to anywhere.
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = var.egress_cidrs
  }
}

resource "aws_instance" "bunker" {
  ami               = "ami-07d0cf3af28718ef8"
  instance_type     = "t2.large"
  availability_zone = "us-east-1a"
  key_name = "${var.ec2_key_name}"
  iam_instance_profile = "${aws_iam_instance_profile.bunker_profile.name}"
  security_groups = ["${aws_security_group.bunker_sg.name}"]
  root_block_device {
    delete_on_termination=false
    volume_size = 20
  }
  tags = {
    Name = "${var.bunker_tag_name}"
  }
  provisioner "file" {
    source      = "install-essentials.sh"
    destination = "/home/ubuntu/install-essentials.sh"
    connection {
      type     = "ssh"
      user     = "ubuntu"
      host = "${self.public_ip}"
      private_key = file("${var.ssh_private_key}")
    }
  }
  provisioner "file" {
    source      = "install-extras.sh"
    destination = "/home/ubuntu/install-extras.sh"
    connection {
      type     = "ssh"
      user     = "ubuntu"
      host = "${self.public_ip}"
      private_key = file("${var.ssh_private_key}")
    }   
  }
  provisioner "file" {
    source      = "prompt.sh"
    destination = "/home/ubuntu/.prompt.sh"
    connection {
      type     = "ssh"
      user     = "ubuntu"
      host = "${self.public_ip}"
      private_key = file("${var.ssh_private_key}")
    }   
  }
  provisioner "file" {
    source      = "init-vnc.sh"
    destination = "/home/ubuntu/init-vnc.sh"
    connection {
      type     = "ssh"
      user     = "ubuntu"
      host = "${self.public_ip}"
      private_key = file("${var.ssh_private_key}")
    }   
  }
  provisioner "file" {
    source      = fileexists("~/.vimrc") ? "~/.vimrc" : "sample-vimrc"
    destination = "/home/ubuntu/.vimrc"
    connection {
      type     = "ssh"
      user     = "ubuntu"
      host = "${self.public_ip}"
      private_key = file("${var.ssh_private_key}")
    }   
  }
  provisioner "remote-exec" {
    inline = [
      "chmod a+x /home/ubuntu/install-essentials.sh",
      "chmod a+x /home/ubuntu/install-extras.sh",
      "chmod a+x /home/ubuntu/.vimrc",
      "chmod a+x /home/ubuntu/init-vnc.sh",
    ]
    connection {
      type     = "ssh"
      user     = "ubuntu"
      host = "${self.public_ip}"
      private_key = file("${var.ssh_private_key}")
    }
  }
}

output "bunker_server_id" {
  value = "${aws_instance.bunker.id}"
}

output "bunker_server_arn" {
  value = "${aws_instance.bunker.arn}"
}

output "bunker_server_sg" {
  value = "${aws_instance.bunker.security_groups}"
}

output "bunker_ip" {
  value = "${aws_instance.bunker.public_ip}"
}
